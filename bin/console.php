#!/usr/bin/env php
<?php

if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', realpath(__DIR__.'/../'));
}

require __DIR__ . '/../vendor/autoload.php';

$config = json_decode(
        str_replace(
                '%ROOT_PATH%',
                ROOT_PATH,
                file_get_contents(ROOT_PATH . '/config/config.json')
        ),

        true
);
$app = new \Silex\Application();

require __DIR__ . '/../src/console.php';

$application = $app['console'];

$application->setHelperset(new \Symfony\Component\Console\Helper\HelperSet([
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($app['db']),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($app['orm.em']),
]));

Doctrine\ORM\Tools\Console\ConsoleRunner::addCommands($application);
Doctrine\DBAL\Tools\Console\ConsoleRunner::addCommands($application);

$application->addCommands([
    new \App\Commands\CalcCountUsersCommand(),
    new \App\Commands\GenerateFakeDataCommand()
]);

$application->run();
