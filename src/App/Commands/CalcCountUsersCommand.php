<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 0:04
 */

namespace App\Commands;

use App\Entities\User;
use App\Repositories\UserRepository;
use Doctrine\ORM\EntityManager;
use Knp\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CalcCountUsersCommand
 * @package App\Commands
 */
class CalcCountUsersCommand extends Command
{
    /**
     * Name command in command line
     */
    const
        COMMAND_NAME = 'users:email-info',
        LIMIT_BATCH  = 30000;

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    protected function configure(): Command
    {
        return $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('calculate count domains');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $resultMap = [];
        $resultMap['allEmails'] = 0;
        $users     = [];
        do {
            $users = $this->getUserRepository()
                ->getUsers($this->getLastId($users), self::LIMIT_BATCH);

            $resultMap = $this->calculateDomains($users, $resultMap);
            $this->getEntityManager()->clear();
            $output->writeln('+ '.self::LIMIT_BATCH);
        } while(count($users) === self::LIMIT_BATCH);

        $this->generateResponse($resultMap, $output);
    }


    private function generateResponse(array $resultMap, OutputInterface $output)
    {
        $output->writeln('Report from DB:', OutputInterface::VERBOSITY_NORMAL);
        foreach ($resultMap as $key => $value) {
            $output->writeln(sprintf('Domain %s users %d', $key, $value));
        }
    }

    /**
     * @return UserRepository
     * @throws InvalidArgumentException
     */
    private function getUserRepository(): UserRepository
    {
            return $this->getEntityManager()
                ->getRepository(User::class);
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager(): EntityManager
    {
        return $this->getHelperSet()
            ->get('em')
            ->getEntityManager();
    }

    private function getLastId(array $users): int
    {
        /** @var $users User[] */
        return count($users) === 0 ? 0 : end($users)->getId();
    }

    /**
     * @param array $users
     * @param array $resultMap
     */
    private function calculateDomains(array $users, array $resultMap): array
    {
        /** @var User[] $users */
        foreach ($users as $user) {
            $string  = $user->getEmail();
            $domains = array_unique($this->getDomainsFromString($string));

            foreach ($domains as $domain ) {
                if (!isset($resultMap[$domain])) {
                    $resultMap[$domain] = 0;
                }
                ++ $resultMap[$domain];
                ++ $resultMap['allEmails'];
            }
        }
        return $resultMap;
    }

    /**
     * @param string $email
     * @return array
     */
    public function getDomainsFromString($email): array
    {
        preg_match_all('/@(.*)(,|$)/Usi', $email, $res);
        return $res[1];
    }
}
