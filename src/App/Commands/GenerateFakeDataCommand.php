<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 1:59
 */

namespace App\Commands;

use App\Entities\User;
use Doctrine\ORM\EntityManager;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Faker\Factory;

class GenerateFakeDataCommand extends Command
{
    /**
     * Name command in command line
     */
    const
        COMMAND_NAME = 'users:generate-data',
        LIMIT_BATCH  = 100000;

    /**
     * @inheritdoc
     * @throws \InvalidArgumentException
     */
    protected function configure(): Command
    {
        return $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('calculate count domains');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $faker = Factory::create();

        for ($i = 1; $i <= self::LIMIT_BATCH; $i++) {
            $user = new User();
            $user->setName($faker->name());
            $user->setGender(random_int(0, 1));

            $emails = [];
            $emailsCount = random_int(1, 5);
            for ($j = 0; $j <= $emailsCount; $j++) {
                $emails[] = $faker->freeEmail;
            }

            $user->setEmail(implode(', ', $emails));
            $this->getEntityManager()->persist($user);
            if ($i % 10000 === 0) {
                $output->writeln('+ 10K users');
                $this->getEntityManager()->flush();
                $this->getEntityManager()->clear();
            }
        }
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager(): EntityManager
    {
        return $this->getHelperSet()
            ->get('em')
            ->getEntityManager();
    }
}
