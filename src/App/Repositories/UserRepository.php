<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 0:28
 */

namespace App\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use App\Entities\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends EntityRepository
{
    /**
     * @var ResultSetMapping
     */
    private $rsm;

    /**
     * @param int $startId
     * @param $limit
     * @return array
     */
    public function getUsers(int $startId, $limit): array
    {
        return $this->getEntityManager()
            ->createNativeQuery(
                'select id, email from users where id > :id limit :limit', $this->getRsm()
            )
            ->setParameters(['id' => $startId, 'limit' => $limit])
            ->getResult();
    }

    /**
     * @return ResultSetMapping
     */
    private function getRsm()
    {
        if (null === $this->rsm) {
            $this->rsm = new ResultSetMapping;
            $this->rsm->addEntityResult(User::class, 'u');
            $this->rsm->addFieldResult('u', 'id', 'id');
            $this->rsm->addFieldResult('u', 'email', 'email');
        }
        return $this->rsm;
    }
}
