<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 23.10.2017
 * Time: 1:03
 */

namespace App\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Class TinyInt
 * @package App\Types
 */
class TinyInt extends Type
{
    const   TINYINT         = 'tinyint',
            DEFAULT_LENGTH  = 2;

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'TINYINT(' . $fieldDeclaration['length'] . ')';
    }


    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }


    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }


    public function getName()
    {
        return self::TINYINT;
    }

    public function getDefaultLength(AbstractPlatform $platform)
    {
        return self::DEFAULT_LENGTH;
    }
}
