<?php
/**
 * Created by PhpStorm.
 * User: kozlov
 * Date: 22.10.2017
 * Time: 23:29
 */

$app->register(new Silex\Provider\MonologServiceProvider(), $config['monolog']);

$app->register(new Knp\Provider\ConsoleServiceProvider(), $config['console']);

$app->register(new \Silex\Provider\DoctrineServiceProvider(), $config['doctrine']['dbs']);

$app->register(new Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), $config['doctrine']['orm']);

Doctrine\DBAL\Types\Type::addType('tinyint', \App\Types\TinyInt::class);

